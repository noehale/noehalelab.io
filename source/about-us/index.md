---
title: About Us
date: 2022-04-10 10:09:11
---

Ferrets are adorable, yes. But they're also full of surprises: they love to explore the house, they can be a bit mischievous at times, and they require some special care and attention. Learn more about ferret care here!
